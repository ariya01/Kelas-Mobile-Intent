package com.pentilku.intent;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import java.io.BufferedReader;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Button btn_kontak = (Button) findViewById(R.id.kontak);
        btn_kontak.setOnClickListener(op);
        Button btn_web = (Button) findViewById(R.id.browser);
        btn_web.setOnClickListener(op);
        Button btn_you = (Button) findViewById(R.id.youtube);
        btn_you.setOnClickListener(op);
        Button btn_galery = (Button) findViewById(R.id.galery);
        btn_galery.setOnClickListener(op);
        Button btn_lagu = (Button) findViewById(R.id.lagu);
        btn_lagu.setOnClickListener(op);
    }

    View.OnClickListener op = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.galery:
                    bukagalery();
                    break;
                case R.id.youtube:
                    bukayou();
                    break;
                case R.id.kontak:
                    bukakontak();
                    Toast.makeText(MainActivity.this, "hai", Toast.LENGTH_SHORT).show();
                    break;
                case R.id.browser:
                    bukabrow();
                    Toast.makeText(MainActivity.this, "hasil", Toast.LENGTH_SHORT).show();
                    break;
                case R.id.lagu:
                    lagu();break;
            }
        }
    };

    public void lagu()
    {
        Intent intent = new Intent(MediaStore.INTENT_ACTION_MUSIC_PLAYER);
        startActivity(intent);
    }

    public void bukagalery() {
        Intent intent = new Intent(getBaseContext(), Main2Activity.class);
        startActivityForResult(intent, 0);
    }

    public void bukayou() {
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://youtube.com"));
        startActivity(intent);
    }

    public void bukakontak() {
        Intent intent = new Intent(Intent.ACTION_CALL);
        intent.setData(Uri.parse("tel:12334"));
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        startActivity(intent);
    }

    public  void  bukabrow()
    {
        Intent intent = new Intent(MainActivity.this,Main3Activity.class);
        startActivity(intent);
    }
}
