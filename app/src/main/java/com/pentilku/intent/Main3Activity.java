package com.pentilku.intent;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class Main3Activity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main3);

        final WebView webView = (WebView)findViewById(R.id.web);
        webView.setWebViewClient(new WebViewClient());
        webView.getSettings().setJavaScriptEnabled(true);

        Button btn_go = (Button)findViewById(R.id.go);
        final EditText editText = (EditText)findViewById(R.id.url);

        btn_go.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String urlku = editText.getText().toString();
                Toast.makeText(Main3Activity.this, "Load This" + urlku, Toast.LENGTH_SHORT).show();
                webView.loadUrl(urlku);
            }
        });
    }
}
